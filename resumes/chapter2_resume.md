Resume of Chapter 2 (Working with the SAE AADL)
===============================================

Software and hardware components have an *(1) interface specification* (component type) and the *(2) blueprint of its implementation* (component implementation). These components can be organized into packages (libraries of components).

* *Packages*, can contain:
	* *Public sections*, specification available to other packages.
	* *Private sections*, implementation details not exposed to other packages.

* *Property sets*, contain new user-defined properties. Standard annexes contain property sets for specific analyses.

* *Software components*, can contain runtime characteristics such as execution times, scheduling, deadlines, execution code size, source code file, language, source code size, etc.
	* *thread*,
	* *process*,
	* *data*.

* *Hardware or execution platform components*, they provide the execution environments, physical communication paths, and external interfaces for a computing system.
	* *processors*,
	* *memory*,
	* *devices*,
	* *buses* (connects the above components).

* The system contains the complete architectural description. It integrates all the composite elements into a single component hierarchy.

* An instance of this top-level system is a mirror image of the system that you are modeling and consequently the operational properties of the system can be analyzed.

Component classifiers
---------------------

In a component **type** declaration you define the category (see ##component-categories) and interfaces (features) of the component. This corresponds to the specification sheet (contract) of the component.

In a component **implementation** declaration you define the internal structure of the component (its constituents and their interaction). This correspond to the blueprint for building a component from its parts.

```
Type + Implementation = Pattern = Component classifier
```
You reference classifiers by their name to establish an occurrence of the pattern that they describe. These references are made within individual textual statements contained within type and implementation declarations.

Component categories
--------------------

Components categories are grouped in:

### Application Software
| Category         | Description |
| :--------------- | :---------- |
| data             | Data in source code and application data types |
| thread           | A schedulable unit of concurrent execution |
| thread group     | An abstraction for logically organizing threads, thread groups, and data components within a process |
| process          | Protected address space enforced at runtime |
| subprogram       | Callable sequentially executable code that represents concepts such as call-return and calls-on methods |
| subprogram group | An abstraction for organizing subprograms into libraries |

### Execution platform (hardware)
| Category         | Description |
| :--------------- | :---------- |
| processor        | Schedules threads and virtual processors |
| virtual processor| Logical resource that is capable of scheduling and executing threads that must be bound to or be a sub-component of one or more physical processors|
| memory           | Stores code and data |
| bus              | Interconnects processors, memory, and devices |
| virtual bus      | Represents a communication abstraction such as a virtual channel or communication protocol |
| device           | Represents sensors, actuators, or other components that interface with external environment |

### Composite
| Category         | Description |
| :--------------- | :---------- |
| system           | Integrates software, hardware, and other system components into a distinct unit within an architecture. It's used to represent composite components. |

### Abstract
| Category         | Description |
| :--------------- | :---------- |
| abstract         | Defines a runtime neutral (conceptual) component that can be refined into another component category. Supports component templates and architecture patterns. |

Summary of AADL declarations
----------------------------

See [./component_contents_resume.ods](./component_contents_resume.ods)