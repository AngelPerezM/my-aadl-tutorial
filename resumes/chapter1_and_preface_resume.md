Resume of Preface and Chapter 1 (MBSSE)
=======================================

Basic concepts
--------------

**Module** is an independent unit that can be used to construct a more complex structure. We (developers) use modules as a way to group logically related code together, classes in OOP and functions in a structured or functional language, and to limit their visibility and use in order to improve modifiability of the software.

**Package** is a mechanism to provide modularity, e.g.: in .NET or C++ namespace is used. 

**Model** is a representation or abstraction that captures the most important aspects of what is being modeled, and hides the details that are not of interest for the design of the system.

**Real-time embedded systems (RTES)** are computational systems within an operational environment that provides monitoring, control, or other critical functions. In these applications systems, the architectural characteristics of vitally importance are *performance, safety, and dependability*. Systems in the automotive, avionics, aerospace, medical, robotics, and process control industries among others fall into this category.

**Model Based Engineering (MBE)**, engineering paradigm that uses models as building blocks throughout a product’s life-cycle. Thanks to MBSE you can predict and understand the capabilities and architectural characteristics (non functional requirements or quality attributes) of the systems and discover system-level problems at an early stage.

**Architecture and Analysis Description Language (AADL)**, it is SAE (Society of Automotive Engineers) International standard and a unifying framework for *MBSE* designed to represent RTE systems. AADL captures the key elements of an embedded software system:

- *Static architecture*, modular software architecture.
- *Dynamic architecture*, runtime architecture in terms of communicating tasks.
- *Computer platform architecture*. I.e. where the software is deployed, processors, memory, and interconnected networks.
- *Physical system or environment*. I.e. the operational environment that interacts with the sw. and hw. of the embedded application.

![](https://learning.oreilly.com/api/v2/epubs/urn:orm:book:9780133132922/files/graphics/f0101.jpg)

With an AADL model you represent the architecture of the system as a hierarchy of interacting components (AADL is a *component-based modeling language*). AADL packages are basically organizers where you can encapsulate interface specifications and implementation blueprints of sw., hw., and physical components.


AADL component model and RTES
-----------------------------

AADL distinguishes between:

- Component interface specifications (AKA: component type declarations, system type).
- Component implementation blueprints (AKA: component implementation declarations, system).
- Component instances (AKA: sub-component declarations).

AADL is a component based modeling language (textual and graphical) used for real-time embedded systems, that not only models software static architecture, but also the hardware for its execution, the operational dynamics of the runtime architecture and its deployment on the computer platform (e.g., how long the sw. takes to execute on a specific processor).

In AADL the models of the *physical characteristics of the system* under control are not explicitly represented, but models and properties of the hardware needed for physical sensing and control appear in AADL.

AADL provides data and subprogram components to abstractly represent application source code implemented in any programming or application language.

AADL provides threads, thread groups, and processes to represent concurrent tasks executing in protected address spaces (time and space partitioning) and interacting through ports, shared data components, and service calls to represent the *software runtime architecture*. The dynamics of the runtime architecture are captured through mode state machines to represent operational modes, dynamic changes to fault-tolerant configurations, and component behavior.

AADL provides processor, memory, and bus components to represent *computer platform architectures* in terms of hardware, and the virtual processor and virtual bus components to represent virtual machines, partitions, virtual channels, and protocols as runtime system abstractions.