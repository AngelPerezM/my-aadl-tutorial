# My AADL tutorial

This repository contains an overview of the Architecture Analysis & Design Language (AADL).
It is a component-based modelling language that supports both textual and graphical representation.

The contents are based on what I have learned from the
["Model-Based Engineering with AADL: An Introduction to the SAE Architecture Analysis & Design Language"](https://www.amazon.es/Model-Based-Engineering-AADL-Introduction-Architecture/dp/0134208897) book.
I.e.: resumes of all chapters and the aadl examples developed with [OSATE](https://osate.org/).
